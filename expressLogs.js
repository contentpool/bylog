var useragent = require('useragent');

module.exports = function(err, req, res) {

	var app = req.app || res.app,
        status = res.statusCode,
        method = req.method,
        url = req.url || '-',
        referer = req.header('referer') || '-',
        ua = useragent.parse(req.header('user-agent')),
        httpVersion = req.httpVersionMajor+'.'+req.httpVersionMinor;
        
    var ip = req.ip || req.connection.remoteAddress ||
            (req.socket && req.socket.remoteAddress) || 
            (req.socket.socket && req.socket.socket.remoteAddresss) ||
            '127.0.0.1';

	var meta = {
        'req_id': req.id,
        'remoteAddress': ip, 
        'method': method,
        'url': url,
        'referer': referer,
        'user-agent': ua,
        'http-version': httpVersion,
        "statusCode": status,
        'req': req,
        'res': res
    };

    if (err) meta.err = err;

    var message = [
            ip, '- -', method, url, 'HTTP/' + httpVersion, status, 
            res.get('Content-Length'), referer, ua.family, ua.major + '.' + ua.minor, ua.os
        ].join(' ');

    return {
    	message: message,
    	metadata: meta
    };
}