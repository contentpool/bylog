var log = require('./index').create('Log1');
log.info({foo: 'bar'}, 'On Log1');

var log2 = require('./index').log;
log2.info('Log1 again');

var req = {};
log2.logger.attachToRequest()(req, {}, function() {
	req.log.info('Log1 via req object');
});

var log3 = require('./index').create('Log3', {
	stdout: true,
	mongodb: {
		connectionString: 'mongodb://localhost:27017/testlog'
	}
});
log3.info('Log3 -> MongoDB');

log.info('Should be on Log1 again');


setTimeout(function() {
	process.exit(0);	
}, 500);
