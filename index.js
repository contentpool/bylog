var bunyan = require('bunyan'),
	expressLogs = require('./expressLogs');

function Logger() {

	this.create = function(name, options) {
		if (this.log) return new Logger().create(name, options);

		var streams = [];

		if (!options || options.stdout) {
			streams.push({
				stream: process.stdout,
				level: (options && options.stdout && options.stdout.level) ? options.stdout.level : 'info'
			});
		}
		if (options) {
			if (options.file) {
				if (!options.file.path)  throw new Error('No path given.');
				streams.push({
					type: 'file',
					path: options.file.path,
					level: options.file.level || 'info'
				});
			}
			if (options.rotatingFile) {
				if (!options.rotatingFile.path)  throw new Error('No path given.');
				streams.push({
					type: 'rotating-file',
					path: options.rotatingFile.path,
					period: options.rotatingFile.period || '1d',
					count: options.rotatingFile.count || 3,
					level: options.rotatingFile.level || 'info'
				});
			}
			if (options.mongodb) {
				var BunyanMongo = require('bunyan-mongo');
				streams.push({
					type: 'raw',
					level: 'trace',
					stream: new BunyanMongo(options.mongodb.connectionString)
				});
			}
			if (options.logentries) {
				var blogentries = require('bunyan-logentries');
				if (!options.logentries.token) throw new Error('Logentries token missing.');
				streams.push({
					type: 'raw',
					level: (options && options.logentries && options.logentries.level) ? options.logentries.level : 'info',
					stream: blogentries.createStream({token: options.logentries.token})
				});
			}
		}

		this.log = bunyan.createLogger({
			name: name,
			streams: streams,
			serializers: bunyan.stdSerializers
		});

		this.log.logger = this;
		return this.log;
	};
}

Logger.prototype.attachToRequest = function() {
	var _this = this;
	return function(req, res, next) {
		req.log = _this.log;
		next();
	};
};

Logger.prototype.requestLogger = function() {
	var _this = this;
	return function(req, res, next) {
		var logEntry = expressLogs(null, req, res);
		_this.log.info(logEntry.metadata, logEntry.message);
        next();
    };
};

Logger.prototype.responseLogger = function() {
	var _this = this;
	return function(req, res, next) {
		var logEntry = expressLogs(null, req, res);
		if (logEntry.metadata.err || logEntry.metadata.statusCode >= 500) {
	        logFn = _this.log.error;
	    } else if (logEntry.metadata.statusCode >= 400) {
	        logFn = _this.log.warn;
	    } else {
	        logFn = _this.log.info;
	    }

        res.on('finish', function() {
            logFn.call(_this.log, logEntry.metadata, logEntry.message);
        });
        next();
    };
};

Logger.prototype.errorLogger = function() {
	var _this = this;
	return function(err, req, res, next) {
		var logEntry = expressLogs(err, req, res);
        _this.log.error(logEntry.metadata, logEntry.message);
        next(err);
    };
};

Logger.prototype.Logger = Logger;

module.exports = new Logger();
