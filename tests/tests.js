var fs = require('fs'),
	expect = require('chai').expect,
	express = require('express'),
	request = require('supertest'),
	_ = require('lodash'),
	interceptStdout = require('./interceptStdout');

describe('bylog', function() {

	var stdout = [];
	var jsonLogs = [];
	var resetStdout;

	before(function() {
		resetStdout = interceptStdout(function(string, encoding, fd) {
			stdout.push(string);
			try { jsonLogs.push(JSON.parse(string)); }
			catch(Error) { }
		});
	});

	after(function() {
		resetStdout();
	});

	it('should return log object', function() {
		var log = require('../index').create('Test Logger');
		expect(log).to.be.not.null;
		expect(log).to.respondTo('info');
		expect(log).to.respondTo('warn');
		expect(log).to.respondTo('error');
	});

	it('should have logger object attached on log object to be able to create more logs', function() {
		var log = require('../index').create('Test Logger');
		expect(log.logger).to.be.not.null;
		expect(log.logger).to.respondTo('create');
	});

	it('should provide access to log singleton via log property', function() {
		require('../index').create('Test Logger');
		var log = require('../index').log;
		expect(log).to.be.not.null;
		expect(log).to.respondTo('info');
		expect(log.fields.name).to.equal('Test Logger');
	});

	it('should be able to create additional loggers without compromising the original logger', function() {
		var log1 = require('../index').create('Test Logger');
		var log2 = require('../index').create('Test Logger 2');

		expect(log1.fields.name).to.equal('Test Logger');
		expect(log2.fields.name).to.equal('Test Logger 2');
		expect(require('../index').log.fields.name).to.equal('Test Logger');
	});

	it('should log to std out if no options passed', function() {
		var log = require('../index').create('Test Logger');
		log.info('Test Message');

		expect(JSON.stringify(stdout)).to.include('Test Message');
	});

	describe('File logging', function() {
		it('should log to file if options.file is set', function(done) {
			var path = '/tmp/bylog-test' + Date.now();
			var log = require('../index').create('Test Logger', {
				file: { path: path }
			});
			var msg = 'Test Log' + Date.now();
			log.info(msg);
			setTimeout(function() {
				var content = fs.readFileSync(path, { encoding: 'utf8'});
				fs.unlinkSync(path);
				expect(content).to.include(msg);
				done();
			}, 300)
		});
	});

	describe('MongoDB logging', function() {

		it('should log to mongo if options.mongo is set', function(done) {
			var connectionString = 'mongodb://localhost:27017/bylog_test';
			var log = require('../index').create('Test Logger', {
				mongodb: {
					connectionString: connectionString
				}
			});
			var msg = 'Test Log' + Date.now();
			log.info(msg);

			setTimeout(function() {
				var MongoClient = require('mongodb').MongoClient;
				MongoClient.connect(connectionString, function(err, db) {
					db.collection('logs').count({msg: msg}, function(err, count) {
						expect(count).to.equal(1);
						done();
					})
				});
			}, 100);
		});

		it('should throw exception if options.mongo.connectionString is not set', function() {
			var connectionString = 'mongodb://localhost:27017/bylog_test';
			expect(function() { require('../index').create('Test Logger', {
					mongodb: { }
				});
			}).to.throw(Error);
		});
	});

	describe('Logentries loggin', function() {

		it('should log to logentries if options.logentries is set', function() {
			var log = require('../index').create('Test Logger', {
				logentries: { token: 'abc' }
			});
			expect(log.streams[0]).to.exist;
		});

		it('should throw exception if logentries token is missing', function() {
			expect(function() { require('../index').create('Test Logger', {
					logentries: { }
				});
			}).to.throw(Error);
		});
	});

	describe('request logger', function() {
		var app, log;

		before(function() {
			log = require('../index').create('Test Logger');
			app = express();
			app.use(log.logger.create('Request Logger').logger.requestLogger());
		});

		it('should be able to call middleware on logger object', function() {
			expect(log.logger).to.respondTo('requestLogger');
		});

		it('should log http request', function(done) {
			request(app).get('/test').end(function(err, res) {
				var hasLogMsgs = _.some(jsonLogs, {
					'name': 'Request Logger',
					'level': 30 ,
					'msg': '127.0.0.1 - - GET /test HTTP/1.1 200  - Other 0.0 Other'
				});
				expect(hasLogMsgs).to.be.true;
				done();
			});
		});
	});

	describe('response logger', function() {
		var app, log;

		before(function() {
			log = require('../index').create('Test Logger');
			app = express();
			app.use(log.logger.create('Response Logger').logger.responseLogger());
			app.use(function(req, res) { res.send(200); });
		});

		it('should be able to call middleware on logger object', function() {
			expect(log.logger).to.respondTo('requestLogger');
		});

		it('should log http request', function(done) {
			request(app).get('/test').end(function(err, res) {
				var hasLogMsgs = _.some(jsonLogs, {
					'name': 'Response Logger',
					'level': 30 ,
					'msg': '127.0.0.1 - - GET /test HTTP/1.1 200  - Other 0.0 Other'
				});
				expect(hasLogMsgs).to.be.true;
				done();
			});
		});
	});

	describe('error logger', function() {
		var app, log;

		before(function() {
			log = require('../index').create('Test Logger');
			app = express();
			app.use(function(req, res, next) {
				return next(new Error('Something went wrong.'));
			});
			app.use(log.logger.create('Error Logger').logger.errorLogger());
		});

		it('should be able to call middleware on logger object', function() {
			expect(log.logger).to.respondTo('errorLogger');
		});

		it('should log http request', function(done) {
			request(app).get('/test').end(function(err, res) {
				var errLog = _.find(jsonLogs, {
					'name': 'Error Logger',
					'level': 50,
				});
				expect(errLog).to.be.not.null;
				expect(errLog.err).to.exist;
				expect(errLog.err.message).to.equal('Something went wrong.');
				done();
			});
		});
	});

	describe('attachToRequest middleware', function() {
		it('should be able to call middleware on logger object', function() {
			var log = require('../index').create('Test Logger');
			expect(log.logger).to.respondTo('attachToRequest');
		});

		it('should attach logger to request object', function(done) {
			var log = require('../index').create('Test Logger');
			var req = {};
			log.logger.attachToRequest()(req, {}, function() {
				expect(req).to.have.property('log');
				expect(req.log).to.respondTo('info');
				done();
			})
		});
	});
});
