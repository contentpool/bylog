function interceptStdout(callback) {
    var old_write = process.stdout.write;
    var old_err_write = process.stderr.write;
 
    process.stdout.write = (function(write) {
        return function(string, encoding, fd) {
            write.apply(process.stdout, arguments)
            callback(string, encoding, fd)
        }
    })(process.stdout.write);

    process.stderr.write = (function(write) { return function(string, encoding, fd) {} })(process.stderr.write);
 
    return function() {
        process.stdout.write = old_write;
        process.stderr.write = old_err_write;
    }
}

module.exports = interceptStdout;