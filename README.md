# Usage

A wrapper for bunyan with a few convenience functions.

### Create logger

```
var log = require('bylog').create('Log 1');
log.info({foo: 'bar'}, 'My log entry');
```

A previosuly created logger can be accessed from any module by

```
var log = require('bylog').log;
```

#### Configuration

```
var log = require('bylog').create('Log 1', {
   stdout: true,
   mongodb: {
      connectionString: 'mongodb://localhsot:27017/logs'
   },
   file: {
      path: '/var/log/log.log'
   },
   rotatingFile: {
      path: '/var/log/rotating.log',
      period: '1d',
      count: 5,
      level: 'info'
   },
   logentries: {
      token: 'MY_LE_TOKEN'
   }
});
```



### ExpressJS utilities

Attach a logger to a ExpressJS request

```
app.use(log.logger.attachToRequest());

// can then be used within controllers and middlewares
req.log.info('controller logging');
```

Request logging
```
app.use(log.logger.requestLogger());
```

Response logging
```
app.use(log.logger.responseLogger());
```

Error logging
```
app.use(log.logger.errorLogger());
```

